import sys
import socket
from MySocket.TcpServer import TcpServer


def main():
    server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    #server.setblocking(socket.SOL_SOCKET,socket.SO_REUSEADDR,1)
    server.bind(('127.0.0.1', 13004))
    server.listen()
    print("Python Tcp服务器启动,监听于127.0.0.1:13004")

    conn,addr = server.accept()
    message = conn.recv(1024)
    print(message)
    res = b"hello world"
    
    conn.send(res)
    
    # server = TcpServer()
    # server.init()
    
if __name__ == "__main__":
    main()


