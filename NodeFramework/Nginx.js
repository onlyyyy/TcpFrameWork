const path = require('path');
const fs = require('fs');
const Nginx = require('./Nginx/Nginx')
const os = require('os');
const ini = require('ini');
async function giveFileName() {
    let type = os.platform();
    console.log('type = ',type);
    let fileName;
    if(type == "darwin")
    {
        fileName = "/Users/hideyoshi/Desktop/codes/my-web/BackEnd/run.ini";

    }
    else if(type == "win32")
    {
        fileName = "F:/my-web/BackEnd/run.ini";

    }
    else
    {
        fileName = "./run.ini"

    }
    return fileName;
}


async function readIni(fileName) {
    let file = fs.readFileSync(fileName);
    var Info = ini.parse(file.toString());
    return Info;
}

async function init() {
    const fileName =await  giveFileName();
    let ini = await readIni(fileName);
    let nginx = new Nginx();

    global.instance = {
        ini:ini,
        nginx:nginx,
    }
    await nginx.init();
}

async function main() {
    await init();
}

main();