
exports.getError = (type, strict) => {
    this.toLog("需要获取的error为 ", type);
    if (!constants.errCode[type]) {
        if (type === undefined) {
            type = 'UNKNOWN_ERROR';
        } else {
            throw {
                code: -44944,
                message: type
            }
        }
    }
    if (strict) {
        throw ({
            code: constants.errCode[type].code,
            message: constants.errCode[type].message
        })
    }

    return {
        code: constants.errCode[type].code,
        message: constants.errCode[type].message
    }
}