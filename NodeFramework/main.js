const applyMysql = require('./tools/applyMysql');
const Socket = require('./tcp/socket');

const http = require('./http/HttpManager').http;
const os = require('os');

const fs = require('fs');
const ini = require('ini');
const Koa = require('koa');
const bodyParser = require('koa-bodyparser');
const koaStatic = require('koa-static');
const indexRouter = require('./http/index');
const Cors = require('koa-cors');
const Logger = require('avenir-log');
let Router = require('koa-router');
async function init() {
    let mysqlInstance = new applyMysql();
    let socket = new Socket();


    let file = await giveFileName();
    let ini = await readIni(file);
    let log = new Logger('main','./log');
    log.noDebug();
    global.log = log;
    mysqlInstance.init({
        host:ini.Mysql.host,
        user:ini.Mysql.name,
        database:ini.Mysql.database,
        password:ini.Mysql.password
    })
    global.instance = {
        dbHandler : mysqlInstance,
        socketHandler : socket,
        httpHandler : http,
        ini:ini,
        fileName:file,
    };
    await socket.init("127.0.0.1",ini.main.tcpport);//启动sock
    



}

async function main() {
    await init();
    start();

}


async function giveFileName() {
    let type = os.platform();
    let fileName;
    if(type == "darwin")
    {
        fileName = "/Users/hideyoshi/Desktop/codes/TcpFrameWork/NodeFramework/run.ini";

    }
    else if(type == "win32")
    {
        fileName = "F:/TcpFrameWork/NodeFramework/run.ini";

    }
    else
    {
        fileName = "./run.ini"

    }
    return fileName;
}


async function readIni(fileName) {
    let file = fs.readFileSync(fileName);
    var Info = ini.parse(file.toString());
    return Info;
}


function start() {
    const app = new Koa();
    app.use(koaStatic('/home/only/my-web/FrontEnd/dist'));
    app.use(bodyParser());
    app.use(Cors());
    var router = new Router();
    router.use('',indexRouter);
    app.use(router.routes());
    app.use(router.allowedMethods());

    let port = instance.ini.main.httpport;
    app.listen(port, () => {
        log.debug("http服务器已启动，127.0.0.1:",port);
    })
}


main();