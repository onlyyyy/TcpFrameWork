#ifndef QTCPDEMO_H
#define QTCPDEMO_H
#include "common.h"
#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class QTcpDemo; }
QT_END_NAMESPACE

class QTcpDemo : public QMainWindow
{
    Q_OBJECT

public:
    QTcpDemo(QWidget *parent = nullptr);
    ~QTcpDemo();

private slots:

    void whileconnected();

    void whiledisconnected();

    void displayError(QAbstractSocket::SocketError);
    void on_btn_close_clicked();

    void on_btn_send_clicked();

    void readTcp();

private:
    Ui::QTcpDemo *ui;
    QTcpSocket *socket;
    bool connected;

};
#endif // QTCPDEMO_H
