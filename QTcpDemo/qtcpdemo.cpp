#include "qtcpdemo.h"
#include "ui_qtcpdemo.h"

QTcpDemo::QTcpDemo(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::QTcpDemo)
{
    ui->setupUi(this);
    connected = false;
    socket = new QTcpSocket;
    socket->abort();
    socket->connectToHost("127.0.0.1",13004,QTcpSocket::ReadWrite);
    connect(socket,SIGNAL(connected()),this,SLOT(whileconnected()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(whiledisconnected()));
    connect(socket,&QTcpSocket::readyRead,this,&QTcpDemo::readTcp);
    connect(socket,SIGNAL(error(QAbstractSocket::SocketError)),this,SLOT(displayError(QAbstractSocket::SocketError)));
}

QTcpDemo::~QTcpDemo()
{
    delete ui;
}


void QTcpDemo::on_btn_close_clicked()
{
    close();
}


void QTcpDemo::on_btn_send_clicked()
{
    DJY::EasyQJson easy;
    if(connected == false)
    {
        QMessageBox::warning(nullptr,"警告","连接服务器失败!");
    }
    QString text = "hello world";
    QJsonObject ob;
    ob["type"] = 1001;
    ob["value"] = text;
    QString JsonStr = easy.readObjectReturnQString(ob);
    socket->write(JsonStr.toLocal8Bit());
    ui->textEdit_receive->append("数据已发送给服务器");
    ui->textEdit_send->clear();
    ui->textEdit_send->repaint();
    ui->textEdit_receive->repaint();

}

void QTcpDemo::displayError(QAbstractSocket::SocketError)
{
    qDebug()<<"出错";
    QString error = socket->errorString();
    QMessageBox::warning(nullptr,"警告",error);
}

void QTcpDemo::whileconnected()
{
    connected = true;
}

void QTcpDemo::whiledisconnected()
{
    connected = false;
}

void QTcpDemo::readTcp()
{
    QByteArray arr = socket->readAll();
    QString result = arr;
    ui->textEdit_receive->append("接收到的数据为:"+result);
    ui->textEdit_receive->repaint();
}
